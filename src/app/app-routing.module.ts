import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ComponentaComponent } from './componenta/componenta.component';
import { ComponentbComponent } from './componentb/componentb.component';


const routes: Routes = [
  {path:'',component:ComponentaComponent},
  { path:'displaydata',component:ComponentbComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


