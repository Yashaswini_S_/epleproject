import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'; 

@Component({
  selector: 'app-componentb',
  templateUrl: './componentb.component.html',
  styleUrls: ['./componentb.component.css']
})
export class ComponentbComponent implements OnInit {

  constructor(private router:Router) { }
  data:string="";
 model : any={}; 
  ngOnInit(): void {
   this.model.data=sessionStorage.getItem('name');
  }
onSubmit()
{
  this.router.navigate(['']);
}
}
