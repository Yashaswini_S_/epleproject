import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor() { }

  getFullName(fname:string,lname:string)
  {
    
    return fname+" "+lname;
  }
}
